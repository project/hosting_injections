<?php

/**
 * The hosting_injections service base class.
 */
class Provision_Service_provision_hosting_injections extends Provision_Service{

  public $service = 'provision_hosting_injections';

  /**
   * Add the needed properties to the site context.
   */
  static function subscribe_site($context){
    drush_log('Setting properties injections');
    $context->setProperty('hosting_injections_vhost_apache');
    $context->setProperty('hosting_injections_locals');
  }

}
