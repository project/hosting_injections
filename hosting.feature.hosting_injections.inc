<?php
/**
 * @file
 * Expose the hosting_injections feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_hosting_injections_hosting_feature() {
  $features['hosting_injections'] = array(
    'title' => t('Hosting injections'),
    'description' => t('Allows injection of arbitary text into settings.php and Apache vhosts'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_injections',
    'group' => 'experimental',
  );
  return $features;
}
