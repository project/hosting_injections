<?php

/**
 * @file
 * Functions for injecting into settings.php and Apache vhosts.
 */

/**
 * Implements hook_form_alter().
 */
function hosting_injections_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'site_node_form') {
    $node = $form['#node'];
    $form['hosting_injections'] = array(
      '#type' => 'fieldset',
      '#title' => t('Hosting injections'),
      '#description' => t('Settings to add to settings.php and apache vhost. WARNING! Completely unvalidated and will break your site if wrong.'),
    );
    $form['hosting_injections']['hosting_injections_locals'] = array(
      '#type' => 'textarea',
      '#title' => 'settings.php',
      '#access' => user_access('edit settings'),
      '#description' => t('Inject settings into settings.php.'),
      '#default_value' => isset($node->hosting_injections_locals) ? $node->hosting_injections_locals : '',
    );
    $form['hosting_injections']['hosting_injections_vhost_apache'] = array(
      '#type' => 'textarea',
      '#title' => 'vhost Injections (Apache)',
      '#access' => user_access('edit vhost'),
      '#description' => t('Inject settings into vhost.'),
      '#default_value' => isset($node->hosting_injections_vhost_apache) ? $node->hosting_injections_vhost_apache : '',
    );
  }
}

/**
 * Implements hook_node_insert().
 */
function hosting_injections_node_insert($node) {
  if ($node->type === 'site') {
    $node->configs = array(
      'hosting_injections_locals' => isset($node->hosting_injections_locals) ? $node->hosting_injections_locals : '',
      'hosting_injections_vhost_apache' => isset($node->hosting_injections_vhost_apache) ? $node->hosting_injections_vhost_apache : '',
    );
    drupal_write_record('hosting_injections', $node);
  }
}

/**
 * Implements hook_node_update().
 */
function hosting_injections_node_update($node) {
  if ($node->type === 'site') {

    $record = array(
      'nid' => $node->nid,
      'vid' => $node->vid,
      'configs' => array(
        'hosting_injections_locals' =>
          isset($node->hosting_injections_locals) ? $node->hosting_injections_locals : '',
        'hosting_injections_vhost_apache' =>
          isset($node->hosting_injections_vhost_apache) ? $node->hosting_injections_vhost_apache : '',
      ),
    );
    $count = db_select('hosting_injections', 'h')
      ->condition('nid', $node->nid)
      ->condition('vid', $node->vid)
        ->countQuery()->execute()->fetchField();

    $keys = array();
    if ($count > 0) {
      $keys = array('nid', 'vid');
    }
    drupal_write_record('hosting_injections', $record, $keys);
  }
}

/**
 * Implements hook_node_delete().
 */
function hosting_injections_node_delete($node) {
  if ($node->type === 'site') {
    $query = db_delete('hosting_injections')
      ->condition('nid', $node->nid);

    return $query->execute();
  }
}

/**
 * Implements hook_node_revision_delete().
 */
function hosting_injections_node_revision_delete($node) {
  if ($node->type === 'site') {
    $query = db_delete('hosting_injections')
      ->condition('nid', $node->nid)
      ->condition('vid', $node->vid);

    return $query->execute();
  }
}

/**
 * Implements hook_node_load().
 */
function hosting_injections_node_load($nodes, $types) {
  if (in_array('site', $types)) {
    foreach ($nodes as $nid => $node) {
      if ($node->type === 'site') {

        $query = db_select('hosting_injections', 'h')
          ->fields('h', array('configs'))
          ->condition('nid', $node->nid)
          ->condition('vid', $node->vid);

        $conf = unserialize($query->execute()->fetchField());

        if (is_array($conf)) {
          $node->hosting_injections_locals = $conf['hosting_injections_locals'];
          $node->hosting_injections_vhost_apache = $conf['hosting_injections_vhost_apache'];
        }
        else {
          $node->hosting_injections_locals = '';
          $node->hosting_injections_vhost_apache = '';
        }
      }
    }
  }
}

/**
 * Implements hook_permission().
 */
function hosting_injections_permission() {
  return array(
    'edit vhost' => array(
      'title' => t('Edit Virtual Host Injections'),
      'description' => t('Allow a user to manage the coded injected in the website VirtualHost'),
      'restrict access' => TRUE,
      'warning' => t("This can completely break not only your site, but also the Aegir hosting interface!"),
    ),
    'edit settings' => array(
      'title' => t('Edit Settings Injections'),
      'description' => t('Allow a user to manage the coded injected in the website setting.php'),
      'restrict access' => TRUE,
      'warning' => t("This can completely break and/or compromise the website."),
    ),
  );
}
