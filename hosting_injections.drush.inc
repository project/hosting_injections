<?php

/**
 * Implementation of hook_hosting_site_context_options().
 */
function hosting_injections_hosting_site_context_options(&$task){
  if (isset($task->ref->hosting_injections_vhost_apache) && !empty($task->ref->hosting_injections_vhost_apache)){
    $task->context_options['hosting_injections_vhost_apache'] = $task->ref->hosting_injections_vhost_apache;
  }
  else{
    $task->context_options['hosting_injections_vhost_apache'] = 'null';
  }
  if (isset($task->ref->hosting_injections_locals) && !empty($task->ref->hosting_injections_locals)){
    $task->context_options['hosting_injections_locals'] = $task->ref->hosting_injections_locals;
  }
  else{
    $task->context_options['hosting_injections_locals'] = 'null';
  }
}

/**
 * Implementation of hook_drush_context_import().
 */
function hosting_injections_drush_context_import($context, &$node){
  if ($context->type == 'site'){
    if (isset($context->hosting_injections_vhost_apache) && !empty($context->hosting_injections_vhost_apache)){
      $node->hosting_injections_vhost_apache = $context->hosting_injections_vhost_apache;
    }
    if (isset($context->hosting_injections_locals) && !empty($context->hosting_injections_locals)){
      $node->hosting_injections_locals = $context->hosting_injections_locals;
    }
  }
}
